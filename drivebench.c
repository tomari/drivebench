/*******
 * Drive speed benchmark -- TOMARI, Hisanobu
 *******/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "machdep.h"

/* data types */
struct bench_options {
  char drive[PATH_MAX+1];
  int points;
  size_t bufsize;
};

/* function prototypes */
static void fill_defaults(struct bench_options *bopts);
static int parse_options(struct bench_options *bopts,int argc,char **argv);
static int run_bench(struct bench_options *bopts);
extern int main(int argc,char **argv);

/* useful macros */
#define TV2DBL(tv) (((double)((tv).tv_sec))+((double)((tv.tv_usec)))/1.E6)

/* implementation */
static void fill_defaults(struct bench_options *bopts)
{
  memset(&(bopts->drive),0,PATH_MAX+1);
  bopts->points=64;
  bopts->bufsize=1024*1024;
  return;
}

static int parse_options(struct bench_options *bopts,int argc,char **argv)
{
  if(argc<2)
    {
      fprintf(stderr,"usage: %s (block device)\n",argv[0]);
      return -1;
    }
  realpath(argv[1],bopts->drive);
  return 0;
}

static int run_bench(struct bench_options *bopts)
{
  int dev,result,sector_size;
  int64_t size;
  int readsize;
  char *buf;

  result=0;
  if((dev=open(bopts->drive,O_RDONLY))<0) /* open disk device */
    {
      perror("open");
      return -1;
    }
  /* get disk information */
  size=md_sizeofblkdev(dev); /* bytes */
  sector_size=md_sizeofsector(dev); /* bytes */
  if(size<0 || sector_size<0)
    result=-1;
  else
    {
      int bufsize;
      unsigned int i;
      int64_t skip;

      /* round the buffer size to multiply of block size */
      bufsize=((bopts->bufsize)/sector_size)*sector_size+
	((bopts->bufsize%sector_size)==0?0:sector_size);
      readsize=size/bopts->points/8;

      /* print what is to be done */
      fprintf(stderr,
	      "Test speed of %s with buffer = %d bytes\n"
	      "read %d bytes for each position\n",bopts->drive,bufsize,readsize);

      /* allocate buffer */
      if((buf=calloc(bufsize,sizeof(char)))==NULL)
	{
	  close(dev);
	  perror("calloc");
	  return -1;
	}

      /* benchmark */
      skip=size/(bopts->points); /* bytes */
      for(i=0; i<bopts->points; i++)
	{
	  struct timeval seek_start,seek_end,read_start,read_end;

	  off_t nextpos;
	  long last_read;
	  long bytes_read;
	  long long headpos;
	  double seektime, readtime;
	  double readspeed;

	  nextpos=i*skip;
	  fprintf(stderr,"%d/%d offset=%lld\n",i,bopts->points,(long long)nextpos);
	  /* seek the head */
	  gettimeofday(&seek_start,NULL);
	  headpos=lseek(dev,nextpos,SEEK_SET);
	  gettimeofday(&seek_end,NULL);
	  if(headpos==(off_t)-1)
	    {
	      perror("lseek");
	      goto bench_end;
	    }

	  /* read data */
	  bytes_read=0;
	  gettimeofday(&read_start,NULL);
	  while(bytes_read<readsize && (last_read=read(dev,buf,bufsize))>0)
	    bytes_read+=last_read;
	  gettimeofday(&read_end,NULL);
	  if(bytes_read<0)
	    perror("read");
	  
	  /* calc time */
	  seektime=(TV2DBL(seek_end)-TV2DBL(seek_start)) *1.E3;
	  readtime=TV2DBL(read_end)-TV2DBL(read_start);
	  /* calc speed */
	  readspeed=(double)bytes_read /readtime;
	  printf("%lld %.13g %.13g\n",(long long)headpos,readspeed,seektime);
	}
    bench_end:
      free(buf);
    }

  close(dev);
  return result;
}

extern int main(int argc,char **argv)
{
  static struct bench_options bopts;
  fill_defaults(&bopts);
  if(parse_options(&bopts,argc,argv)<0)
    exit(EXIT_FAILURE);
  if(run_bench(&bopts)<0)
    exit(EXIT_FAILURE);
  exit(EXIT_SUCCESS);
  return EXIT_FAILURE;
}
