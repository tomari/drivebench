CFLAGS=-Wall -D_FILE_OFFSET_BITS=64

drivebench: drivebench.o machdep.o
	$(CC) $(CFLAGS) -o drivebench $^

machdep.o: machdep.c machdep.h
	$(CC) $(CFLAGS) -o machdep.o -c machdep.c

drivebench.o: drivebench.c
	$(CC) $(CFLAGS) -o drivebench.o -c drivebench.c

clean:
	rm -f drivebench *.o
