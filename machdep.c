/*******
 * machdep.c -- TOMARI, Hisanobu
 * This version is for Linux
 *******/
#include <stropts.h>
#include <linux/ioctl.h>
#include <linux/hdreg.h>
#include <linux/fs.h>
#include "machdep.h"

/* return size of block device to which the file descriptor blkdev points
   in bytes. */
extern int64_t md_sizeofblkdev(int blkdev)
{
  int64_t blksz;
  if(ioctl(blkdev,BLKGETSIZE64,&blksz)<0)
    blksz=-1;
  return blksz;
}

extern int md_num_sectors(int blkdev)
{
  int sectors;
  if(ioctl(blkdev,BLKGETSIZE,&sectors)<0)
    sectors=-1;
  return sectors;
}

extern int md_sizeofsector(int blkdev)
{
  int sectorsize;
  if(ioctl(blkdev,BLKBSZGET,&sectorsize)<0)
    sectorsize=-1;
  return sectorsize;
}
